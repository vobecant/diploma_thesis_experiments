import torch
from torch import nn,optim
from functions import *
from networks import *
from training import *
import sys

use_cuda = torch.cuda.is_available()
net_type = sys.argv[1]
print('use_cuda: {}, net_type: {}'.format(use_cuda,net_type))

# Load data
data = mnist_data()
# Create loader with data, so that we can iterate over it
data_loader = torch.utils.data.DataLoader(data, batch_size=100, shuffle=True)
# Num batches
num_batches = len(data_loader)

# create networks
discriminator = DiscriminatorNet(net_type)
generator = GeneratorNet(net_type)
if use_cuda:
    discriminator = discriminator.cuda()
    generator = generator.cuda()

# optimization - use Adam with learning rate 0.0002
learning_rate = 0.0001
d_optimizer = optim.Adam(discriminator.parameters(), lr=learning_rate)
g_optimizer = optim.Adam(generator.parameters(), lr=learning_rate)

# loss function - Binary cross entropy (BCE)
loss = nn.BCELoss()

# train GAN
train_net(data_loader,discriminator,generator,d_optimizer,g_optimizer,loss,use_cuda,net_type=='conv',200)
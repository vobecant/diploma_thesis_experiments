from functions import *
from utils import Logger
from visualization import plot_loss
from torch.autograd.variable import Variable
import numpy as np

# Create logger instance
logger = Logger(model_name='VGAN', data_name='MNIST')


def train_discriminator(discriminator, optimizer, loss, real_data, fake_data, use_cuda):
    # how many data samples do we have
    N = real_data.size(0)
    # Reset gradients
    optimizer.zero_grad()

    ones = ones_target(N).cuda() if use_cuda else ones_target(N)
    zeros = zeros_target(N).cuda() if use_cuda else zeros_target(N)

    # 1.1 Train on Real Data
    prediction_real = discriminator(real_data)
    # Calculate error and backpropagate
    error_real = loss(prediction_real, ones)
    error_real.backward()

    # 1.2 Train on Fake Data
    prediction_fake = discriminator(fake_data)
    # Calculate error and backpropagate
    error_fake = loss(prediction_fake, zeros)
    error_fake.backward()

    # 1.3 Update weights with gradients
    optimizer.step()

    # Return error and predictions for real and fake inputs
    return error_real + error_fake, prediction_real, prediction_fake


def train_generator(discriminator, optimizer, loss, fake_data, use_cuda):
    # how many fake data samples do we have
    N = fake_data.size(0)

    # reset gradients
    optimizer.zero_grad()

    # sample noise and generate fake data
    prediction = discriminator(fake_data)

    ones = ones_target(N).cuda() if use_cuda else ones_target(N)

    # calculate error and backpropagate
    error = loss(prediction, ones)
    error.backward()

    # update weights with gradients
    optimizer.step()

    # return error
    return error




def train_net(data_loader, discriminator, generator, d_opt, g_opt, loss, use_cuda, conv=False, n_epochs=100):

    num_batches = len(data_loader)
    num_test_samples = 16
    # static batch of noise, every few steps we will visualize
    # the batch of images the generator outputs when using this noise as input
    test_noise = noise(num_test_samples)
    if use_cuda:
        test_noise = test_noise.cuda()
    Dloss = []
    Gloss = []

    for epoch in range(n_epochs):
        Dloss_ep = []
        Gloss_ep = []
        for n_batch, (real_batch, _) in enumerate(data_loader):
            N = real_batch.size(0)

            # 1. train discriminator
            if conv:
                if use_cuda:
                    real_data = Variable(real_batch.cuda())
                else:
                    real_data = Variable(real_batch)
            else:
                if use_cuda:
                    real_data = Variable(images_to_vectors(real_batch).cuda())
                else:
                    real_data = Variable(images_to_vectors(real_batch))

            # generate fake data and detach -> so gradients are NOT computed for generator
            if use_cuda:
                fake_data = generator(noise(N).cuda()).cuda().detach()
            else:
                fake_data = generator(noise(N)).detach()

            # train D
            d_error, d_pred_real, d_pred_fake = train_discriminator(discriminator, d_opt, loss, real_data, fake_data, use_cuda)
            Dloss_ep.append(d_error.item())
            # 2. train generator

            # generate NEW fake data - other than we used when training D
            if use_cuda:
                fake_data = generator(noise(N).cuda()).cuda()
            else:
                fake_data = generator(noise(N))

            # train G
            g_error = train_generator(discriminator, g_opt, loss, fake_data,use_cuda)
            Gloss_ep.append(g_error.item())
            # log batch error
            logger.log(d_error,g_error,epoch,n_batch,num_batches)

            # Display Progress every few batches
            if (n_batch) % 300 == 0:
                test_images = vectors_to_images(generator(test_noise))
                test_images = test_images.data
                logger.log_images(
                    test_images, num_test_samples,
                    epoch, n_batch, num_batches
                );
                # Display status Logs
                logger.display_status(
                    epoch, n_epochs, n_batch, num_batches,
                    d_error, g_error, d_pred_real, d_pred_fake
                )
        Dloss.append(np.mean(Dloss_ep))
        Gloss.append(np.mean(Gloss_ep))
        plot_loss(Dloss,Gloss)
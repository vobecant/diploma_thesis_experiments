import sys
import torch
from torch import nn, optim
from functions import *
from networks import *
from training import *

use_cuda = torch.cuda.is_available()
net_type = sys.argv[1]
n_epochs = int(sys.argv[2])
variance = float(sys.argv[3])
print('use_cuda: {}, net_type: {}, number of epochs: {}, maximal variance of input noise: {}'.format(use_cuda,
                                                                                                     net_type,
                                                                                                     n_epochs,
                                                                                                     variance))

# Load data
data = mnist_data()
# Create loader with data, so that we can iterate over it
data_loader = torch.utils.data.DataLoader(data, batch_size=100, shuffle=True)
# Num batches
num_batches = len(data_loader)

# create networks
discriminator = DiscriminatorNet(net_type)
generator = GeneratorNet(net_type)
if use_cuda:
    discriminator = discriminator.cuda()
    generator = generator.cuda()

# optimization - use Adam with learning rate 0.0002
learning_rate = 0.0001
# if net_type=='linear':
#    d_optimizer = optim.Adam(discriminator.parameters(), lr=learning_rate)
# else:
#    d_optimizer = optim.SGD(discriminator.parameters(), lr=learning_rate)
d_optimizer = optim.Adam(discriminator.parameters(), lr=learning_rate)
g_optimizer = optim.Adam(generator.parameters(), lr=learning_rate)

# loss function - Binary cross entropy (BCE)
loss = nn.BCELoss()

# train GAN
train_net(data_loader, discriminator, generator, d_optimizer, g_optimizer, loss, use_cuda, net_type == 'conv',
          n_epochs=n_epochs, soft_labels=True, add_noise=True, variance=variance)

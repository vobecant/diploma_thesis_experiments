import torch
from torch.autograd.variable import Variable
from torchvision import transforms, datasets
import numpy as np

def mnist_data():
    compose = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((.5, .5, .5), (.5, .5, .5))
        ])
    out_dir = './dataset'
    return datasets.MNIST(root=out_dir, train=True, transform=compose, download=True)

def images_to_vectors(images):
    return images.view(images.size(0), 784)

def vectors_to_images(vectors):
    return vectors.view(vectors.size(0), 1, 28, 28)

def noise(size, conv=False):
    '''
    Generates a 1-d vector of gaussian sampled random values (mean 0, variance 1)
    '''
    if conv:
        n = Variable(torch.randn(size,10,10))
    else:
        n = Variable(torch.randn(size, 100))
    return n

noisy_labels = lambda n_labels,lb,ub: torch.from_numpy(np.random.uniform(lb,ub,(n_labels,1))).float()

def ones_target(size,soft=False):
    '''
    Tensor containing ones, with shape = size
    '''
    data = Variable(torch.ones(size, 1))
    if soft:
        # LB and UB (in link is proposed 1.2) from https://github.com/soumith/ganhacks
        data = noisy_labels(data.size(0),0.7,1.0)
    return data

def zeros_target(size,soft = False):
    '''
    Tensor containing zeros, with shape = size
    '''
    data = Variable(torch.zeros(size, 1))
    if soft:
        # LB and UB from https://github.com/soumith/ganhacks
        data = noisy_labels(data.size(0),0.0,0.3)
    return data
import matplotlib.pyplot as plt

def plot_loss(Dloss,Gloss):
    file = './images/'
    steps = range(len(Dloss))
    fig = plt.figure()
    plt.plot(steps,Dloss)
    plt.title('Discriminator loss')
    plt.xlabel('iteration')
    plt.ylabel('loss')
    fig.savefig('{}Dloss.png'.format(file))
    fig = plt.figure()
    plt.plot(steps, Gloss)
    plt.title('Generator loss')
    plt.xlabel('iteration')
    plt.ylabel('loss')
    fig.savefig('{}Gloss.png'.format(file))
    plt.close('all')
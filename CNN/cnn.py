import os.path

import numpy as np
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms

from CNN import functions

find = lambda searchList, elem: [[i for i, x in enumerate(searchList) if x == e] for e in elem]
flatten = lambda list: [item for sublist in list for item in sublist]
find_idx = lambda classes, dataset: flatten(find(classes, dataset))
transform_labels = lambda classes, labels: [np.where(classes == l) for l in labels]

fname = './model.ckpt'
train_model = True
run_test = True
load = False
max_train = 0

# Device configuration
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

# Hyper parameters
classes = [6, 8]
num_classes = len(classes)
batch_size = 100
train_size = 10000

# MNIST dataset
train_dataset = torchvision.datasets.MNIST(root='../../data/',
                                           train=True,
                                           transform=transforms.ToTensor(),
                                           download=True)
train_dataset = functions.limit_dataset(train_dataset, classes, True, train_size)
print('train data: ' + str(len(train_dataset.train_data)) + ', train labels:' + str(len(train_dataset.train_labels)))

test_dataset = torchvision.datasets.MNIST(root='../../data/',
                                          train=False,
                                          transform=transforms.ToTensor())
test_dataset = functions.limit_dataset(test_dataset, classes, False)
print('test data: ' + str(len(test_dataset.test_data)) + ', test labels:' + str(len(test_dataset.test_labels)))

# Data loader
train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                           batch_size=batch_size,
                                           shuffle=True)

test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                          batch_size=batch_size,
                                          shuffle=False)


# Convolutional neural network (two convolutional layers)
class ConvNet(nn.Module):
    def __init__(self, num_classes=10):
        super(ConvNet, self).__init__()
        self.layer1 = nn.Sequential(
            # torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding)
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.fc = nn.Linear(7 * 7 * 32, num_classes)
        self.softmax = nn.Softmax(1)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.fc(out)
        out = self.softmax(out)
        return out

    def forward_conv(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        return out.detach().numpy()

# loss
criterion = nn.CrossEntropyLoss()

def train(load,model,learning_rate,num_epochs):
    if load and os.path.isfile(fname):
        model.load_state_dict(torch.load(fname))
        print('loaded')

    if train_model:
        #optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

        # Train the model
        total_step = len(train_loader)
        for epoch in range(num_epochs):
            print("epoch {}".format(epoch))
            for i, (images, labels) in enumerate(train_loader):
                images = images.to(device)
                labels = labels.to(device)

                # Forward pass
                outputs = model(images)
                loss = criterion(outputs, labels)

                # Backward and optimize
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()

                if (i + 1) % 100 == 0:
                    print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'
                          .format(epoch + 1, num_epochs, i + 1, total_step, loss.item()))

        # Save the model checkpoint
        torch.save(model.state_dict(), 'model.ckpt')

# Test the model
def test(model, set = 'test'):
    model.eval()  # eval mode (batchnorm uses moving mean/variance instead of mini-batch mean/variance)
    loader = test_loader if set == 'test' else train_loader
    with torch.no_grad():
        print("Starting evaluation")
        correct = 0
        total = 0
        for images, labels in test_loader:
            images = images.to(device)
            labels = labels.to(device)

            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

        print('{} Accuracy of the model on the {} test images: {}% ({} wrong)'.format(set,len(loader.dataset),100 * correct / total, total - correct))

# functions.svm_closest(train_dataset,conv_out,labels,10,'linear')

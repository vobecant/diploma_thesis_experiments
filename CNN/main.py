import CNN.cnn as cnn

from CNN import functions

learning_rate = 0.001
num_epochs = 10

model = cnn.ConvNet(cnn.num_classes).to(cnn.device)
cnn.train(False, model, learning_rate=learning_rate, num_epochs=num_epochs)
cnn.test(model, 'train')
worst_id, best_id = functions.best_worst_classified(cnn.train_loader, cnn.train_dataset, cnn.device, cnn.classes, model,
                                                    cnn.criterion)

# functions.svm_closest(train_dataset,conv_out,labels,10,'linear')

import numpy as np
import matplotlib.pyplot as plt
import torch

flatten = lambda list: [item for sublist in list for item in sublist]


def find_idx(to_find, array):
    idx = []
    for i, el in enumerate(array):
        for x in to_find:
            if x == el:
                idx.append(i)
    return idx


def limit_dataset(dataset, classes, train=True, size=None):
    labels = dataset.train_labels if train else dataset.test_labels
    idx = find_idx(classes, labels)
    if size is None: size = len(idx)
    idx = idx[:size]
    if train:
        dataset.train_data = dataset.train_data[idx]
        dataset.train_labels = modify_labels(dataset.train_labels[idx], classes)
    else:
        dataset.test_data = dataset.test_data[idx]
        dataset.test_labels = modify_labels(dataset.test_labels[idx], classes)
    return dataset


def modify_labels(labels, classes):
    for i in range(len(classes)):
        cur_class = classes[i]
        idx = find_idx([cur_class], labels)
        labels[idx] = i
    return labels


def conv_flatten(conv):
    conv = np.array(conv)
    conv = conv.flatten()
    return conv


def best_worst_classified(train_loader, train_dataset, device, classes, model, criterion, size=10):
    conv_out = []
    outputs_out = []
    labels_out = []
    predictions = []
    j = 0
    for i, (images, labels) in enumerate(train_loader):
        images = images.to(device)
        labels = labels.to(device)
        labels_out.extend(labels.detach().numpy())
        idx = find_idx(labels, classes)

        # Forward conv pass
        cout = model.forward_conv(images)
        outputs = model(images)
        loss = criterion(outputs, labels)
        _, predicted = torch.max(outputs.data, 1)
        predictions.extend(predicted)
        for o, l, p in zip(outputs, labels, predicted):
            outputs_out.append(o[l])
            #if l != p: print('error in img {}, predicted {}, true {}, true class prob {}'.format(j, p, l, o[l]))
            j += 1
        for c in cout:
            c = conv_flatten(c)
            conv_out.append(c)

    conv_out = np.array(conv_out)
    labels_out = np.array(labels_out)
    labels_out = labels_out.flatten()
    outputs_out = np.array(outputs_out)
    print(conv_out.shape)
    lowest = outputs_out.argsort()[:size]
    largest = outputs_out.argsort()[-size:]
    #print('worst predictions: {}'.format(outputs_out[lowest]))
    #print('best predictions: {}'.format(outputs_out[largest]))
    for i in range(len(largest)):
        id = largest[i]
        im, l = train_dataset[id]
        plt.imshow(im[0, :, :])
        plt.title('id {},target={},true class prediction={}'.format(id, l, outputs_out[id]))
        plt.show()
    for i in range(len(lowest)):
        id = lowest[i]
        im, l = train_dataset[id]
        plt.imshow(im[0, :, :])
        plt.title('id {},target={}, predicted={}, true class pred={}'.format(id, l, predictions[id], outputs_out[id]))
        plt.show()
    return lowest, largest


def svm_closest(train_dataset, data, labels, size, kernel='linear'):
    from sklearn.svm import SVC as svm
    clf = svm(kernel=kernel)
    clf.fit(data, labels)
    x = clf.decision_function(data)
    abs_x = np.abs(x)
    print('decision function: {}, abs: {}'.format(x, abs_x))
    id_min = np.argmin(abs_x)
    id_max = np.argmax(abs_x)
    print('SVM accuracy: {}'.format(clf.score(data, labels)))
    print('min distance: {}, max distance: {}'.format(x[id_min], x[id_max]))
    lowest = abs_x.argsort()[:size]
    largest = abs_x.argsort()[-size:]
    im, l = train_dataset[lowest[0]]
    plt.imshow(im[0, :, :])
    plt.figure()
    im, l = train_dataset[largest[0]]
    plt.imshow(im[0, :, :])
    plt.show()

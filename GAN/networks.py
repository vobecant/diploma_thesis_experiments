import torch
from torch import nn
from GAN.utils import Logger

dim = None

class DiscriminatorNet(torch.nn.Module):
    """
    A three hidden-layer discriminative neural network
    """

    leakyRelu_rate = 0.2
    dropout_rate = 0.3
    net_type = None

    def __init__(self, type='linear'):
        if type == 'linear':
            self.init_linear()
        else:
            self.init_conv()
        self.net_type = type

    def init_linear(self):
        super(DiscriminatorNet, self).__init__()
        n_features = 784  # flattened 28x28 image from MNIST dataset
        n_out = 1  # probability

        self.hidden0 = nn.Sequential(
            nn.Linear(n_features, 1024),  # linear transformation from 784 features to 1014
            nn.LeakyReLU(self.leakyRelu_rate),
            nn.Dropout(self.dropout_rate)
        )

        self.hidden1 = nn.Sequential(
            nn.Linear(1024, 512),
            nn.LeakyReLU(self.leakyRelu_rate),
            nn.Dropout(self.dropout_rate)
        )

        self.hidden2 = nn.Sequential(
            nn.Linear(512, 256),
            nn.LeakyReLU(self.leakyRelu_rate),
            nn.Dropout(self.dropout_rate)
        )

        self.out = nn.Sequential(
            nn.Linear(256, n_out),
            nn.Sigmoid()
        )

    def init_conv(self):
        super(DiscriminatorNet, self).__init__()
        self.hidden0 = nn.Sequential(
            # torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride, padding)
            nn.Conv2d(1, 16, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.hidden1 = nn.Sequential(
            nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.out = nn.Sequential(
            nn.Conv2d(32, 1, kernel_size=7),
            nn.Sigmoid()
        )

    # forward pass of the network
    def forward(self, x):
        x = self.hidden0(x)
        x = self.hidden1(x)
        if self.net_type == 'linear': x = self.hidden2(x)
        x = self.out(x)
        x = x.view(-1, 1)
        return x


class GeneratorNet(torch.nn.Module):
    """
    A three hidden-layer generative neural network.
    The output layer will have a TanH activation function,
    which maps the resulting values into the (-1, 1) range,
    which is the same range in which our preprocessed MNIST images is bounded.
    """

    leakyRelu_rate = 0.2
    n_features = 100  # dimension of input noise
    n_out = 784  # flattened 28x28 MNIST image
    net_type = None
    dim = None

    def __init__(self, type='linear'):
        super(GeneratorNet, self).__init__()
        if type == 'linear':
            self.init_linear()
        else:
            self.init_conv()
        self.net_type = type

    def init_linear(self):
        self.hidden0 = nn.Sequential(
            nn.Linear(self.n_features, 256),
            nn.LeakyReLU(self.leakyRelu_rate)
        )

        self.hidden1 = nn.Sequential(
            nn.Linear(256, 512),
            nn.LeakyReLU(self.leakyRelu_rate)
        )

        self.hidden2 = nn.Sequential(
            nn.Linear(512, 1024),
            nn.LeakyReLU(self.leakyRelu_rate)
        )

        self.out = nn.Sequential(
            nn.Linear(1024, self.n_out),
            nn.Tanh()
        )

    def init_conv(self, d=28):
        self.dim = d
        self.lin = nn.Sequential(
            nn.Linear(100, 4 * 4 * 4 * d),
            nn.ReLU(True)
        )
        self.hidden0 = nn.Sequential(
            nn.ConvTranspose2d(4 * d, 2 * d, 4),
            nn.ReLU(True)
        )

        self.hidden1 = nn.Sequential(
            nn.ConvTranspose2d(2 * d, d, 5),
            nn.ReLU(True)
        )

        self.out = nn.Sequential(
            nn.ConvTranspose2d(d, 1, 8, 2),
            nn.Sigmoid()
        )

    def forward(self, x):
        if self.net_type == 'conv':
            # x = x.view(-1,100,1,1)
            x = self.lin(x)
            x = x.view(-1, 4 * self.dim, 4, 4)
            x = self.hidden0(x)
            x = self.hidden1(x)
        else:
            x = self.hidden0(x)
            x = self.hidden1(x)
            x = self.hidden2(x)
        x = self.out(x)
        return x


class DCGAN_D(torch.nn.Module):
    def __init__(self, d=128, leak_rate=0.2):
        super(DCGAN_D, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, d, 4, 2, 1),
            nn.LeakyReLU(leak_rate)
        )
        self.layer2 = nn.Sequential(
            nn.Conv2d(d, d * 2, 4, 2, 1),
            nn.BatchNorm2d(d * 2),
            nn.LeakyReLU(leak_rate)
        )
        self.layer3 = nn.Sequential(
            nn.Conv2d(d * 2, d * 4, 4, 2, 1),
            nn.BatchNorm2d(d * 4),
            nn.LeakyReLU(0.2)
        )
        self.layer4 = nn.Sequential(
            nn.Conv2d(d * 4, d * 8, 4, 2, 1),
            nn.BatchNorm2d(d * 8),
            nn.LeakyReLU(0.2)
        )
        self.out = nn.Sequential(
            nn.Conv2d(d * 8, 1, 4, 1, 0),
            nn.Sigmoid()
        )

    def weight_init(self, mean, std):
        for m in self._modules:
            normal_init(self._modules[m], mean, std)

    def forward(self, input):
        x = self.layer1(input)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.out(x)
        return x


class DCGAN_G(torch.nn.Module):
    def __init__(self, noise_dim=100, d=128):
        super(DCGAN_G, self).__init__()
        self.layer1 = nn.Sequential(
            nn.ConvTranspose2d(noise_dim, d * 8, 4, 1, 0),
            nn.BatchNorm2d(d * 8),
            nn.ReLU()
        )
        self.layer2 = nn.Sequential(
            nn.ConvTranspose2d(d * 8, d * 4, 4, 2, 1),
            nn.BatchNorm2d(d * 4),
            nn.ReLU()
        )
        self.layer3 = nn.Sequential(
            nn.ConvTranspose2d(d * 4, d * 2, 4, 2, 1),
            nn.BatchNorm2d(d * 2),
            nn.ReLU()
        )
        self.layer4 = nn.Sequential(
            nn.ConvTranspose2d(d * 2, d, 4, 2, 1),
            nn.BatchNorm2d(d),
            nn.ReLU()
        )
        self.out = nn.Sequential(
            nn.ConvTranspose2d(d, 1, 4, 2, 1),
            nn.Tanh()  # so the output is in range (-0.5,0.5) just as the input inmage to discriminator
        )

    def weight_init(self, mean, std):
        for m in self._modules:
            normal_init(self._modules[m],mean,std)

    def forward(self,input):
        input = input.view(-1, 100, 1, 1)
        x = self.layer1(input)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.out(x)
        return x


def normal_init(m,mean,std):
    if isinstance(m, nn.ConvTranspose2d) or isinstance(m,nn.Conv2d):
        m.weight.data.normal_(mean, std)
        m.bias.data.zero_()
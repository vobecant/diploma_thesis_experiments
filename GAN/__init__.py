import torch
from torch import nn,optim
from GAN.functions import *
from GAN.networks import *
from GAN.training import *

use_cuda = torch.cuda.is_available()
net_type = 'conv'

# Load data
data = mnist_data(dcgan=(net_type=='conv'))
# Create loader with data, so that we can iterate over it
data_loader = torch.utils.data.DataLoader(data, batch_size=100, shuffle=True)
# Num batches
num_batches = len(data_loader)

# create networks
if net_type=='conv':
    discriminator = DCGAN_D()#DiscriminatorNet(net_type)
    generator = DCGAN_G()#GeneratorNet(net_type)
else:
    discriminator = DiscriminatorNet(net_type)
    generator = GeneratorNet(net_type)

if use_cuda:
    discriminator = discriminator.cuda()
    generator = generator.cuda()

# optimization - use Adam with learning rate 0.0002
learning_rate = 0.0001
d_optimizer = optim.Adam(discriminator.parameters(), lr=learning_rate, betas=(0.5,0.999))
g_optimizer = optim.Adam(generator.parameters(), lr=learning_rate, betas=(0.5,0.999))

# loss function - Binary cross entropy (BCE)
loss = nn.BCELoss()

# train GAN
train_net(data_loader,discriminator,generator,d_optimizer,g_optimizer,loss,use_cuda,net_type=='conv',200)
import os
import torch


def load_nets(netD, netG, optimizerD, optimizerG, Dnet_file, Gnet_file, architecture):
    if os.path.isfile(Dnet_file):
        print("=> loading checkpoint '{}'".format(Dnet_file))
        checkpoint = torch.load(Dnet_file)
        START_ITER = checkpoint['iteration']
        if architecture == 'wgan':
            WDIST_BEST = checkpoint['Wdist']
        netD.load_state_dict(checkpoint['state_dict'])
        optimizerD.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (iteration {})".format(Dnet_file, checkpoint['iteration']))
    else:
        print("=> no checkpoint found at '{}'".format(Dnet_file))
    if os.path.isfile(Gnet_file):
        print("=> loading checkpoint '{}'".format(Gnet_file))
        checkpoint = torch.load(Gnet_file)
        START_ITER = checkpoint['iteration']
        if architecture == 'wgan':
            WDIST_BEST = checkpoint['Wdist']
        netG.load_state_dict(checkpoint['state_dict'])
        optimizerG.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (iteration {})".format(Gnet_file, checkpoint['iteration']))
    else:
        print("=> no checkpoint found at '{}'".format(Gnet_file))
    if architecture == 'wdist':
        return netD, netG, optimizerD, optimizerG, WDIST_BEST
    else:
        return netD, netG, optimizerD, optimizerG


def save_nets(netD, netG, optimizerD, optimizerG, iteration, Dnet_file, Gnet_file, architecture, Wdist=None):
    Ddict = {'iteration': iteration, 'arch': 'wgan_D', 'state_dict': netD.state_dict(),
             'optimizer': optimizerD.state_dict()}
    if architecture == 'wgan':
        Ddict['Wdist'] = Wdist
    save_checkpoint(Ddict, Dnet_file)
    print('discriminator network saved to {}'.format(Dnet_file))
    Gdict = {'iteration': iteration, 'arch': 'wgan_G', 'state_dict': netG.state_dict(),
             'optimizer': optimizerG.state_dict()}
    if architecture == 'wgan':
        Gdict['Wdist'] = Wdist
    save_checkpoint(Gdict, Gnet_file)
    print('generator network saved to {}'.format(Gnet_file))


def save_checkpoint(state, filename):
    torch.save(state, filename)

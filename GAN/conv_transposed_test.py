import torch
from torch import nn
from torch.autograd.variable import Variable

"""
in_channels, out_channels, kernel_size, stride=1, padding=0, output_padding=0, groups=1, bias=True, dilation=1:

"""

class TestNet(nn.Module):
    noise_dim = 100
    output_dim = [1,28,28]
    d = 128
    def __init__(self):
        super(TestNet,self).__init__()

        # 128x7x7 -> 64x10x10
        self.deconv1 = nn.Sequential(
            nn.ConvTranspose2d(100, 4 * self.d, 4, 1, 0),
            nn.BatchNorm2d(4 * self.d),
            nn.ReLU()
        )

        # 64x10x10 -> 32x13x13
        self.deconv2 = nn.Sequential(
            nn.ConvTranspose2d(4*self.d, 2 * self.d, 4, 2, 1),
            nn.BatchNorm2d(2 * self.d),
            nn.ReLU()
        )

        # 32x13x13 -> 16x16x16
        self.deconv3 = nn.Sequential(
            nn.ConvTranspose2d(2*self.d, 1 * self.d, 4, 2, 2),
            nn.BatchNorm2d(1 * self.d),
            nn.ReLU()
        )

        # 16x16x16 -> 1x18x18
        self.out = nn.Sequential(
            nn.ConvTranspose2d(self.d,1,4,2,1),
            nn.Tanh()
        )

    def forward(self, x):
        x = x.view(-1,100,1,1)
        x = self.deconv1(x)
        x = self.deconv2(x)
        x = self.deconv3(x)
        x = self.out(x)
        return x

net = TestNet()
sample_noise = Variable(torch.randn(1, 100))
net(sample_noise)